<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Blogger</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<!-- /fonts -->
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/trend.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/info.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- /css -->
</head>
<body>
<!-- navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.html"><h1>XFashion</h1></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp">主页</a></li>
				<li><a href="latest.jsp">最新</a></li>
<!--				<li><a href="service.html">分类</a></li>-->
				<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=通勤">分类</a></li>
                <li class="active"><a href="blogger.jsp">博主</a></li>
                <c:if test = "${empty sessionScope.user}">
                <li><a href="login.jsp">登录</a></li>
				</c:if>
				<c:if test = "${!empty sessionScope.user}">
                <li><a href="/fashion/ActionServlet?entity=Owner&method=selectByName">个人主页</a></li>
				</c:if>
			</ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<!-- /navigation -->
<!-- banner section -->
<div id="slider" class="slider-container2">
	<ul class="slider">
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner1.jpg" alt="An Image" draggable="false">
			</div>
		</li>
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner2.jpg" alt="An Image" draggable="false">
			</div>
		</li>
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner3.jpg" alt="An Image" draggable="false">
			</div>
		</li>
	</ul>
	<div class="slider-controls">
		<div class="slide-nav">
			<a href="#" class="prev"><img src="images/prev.png" alt="w3layouts"></a>
			<a href="#" class="next"><img src="images/next.png" alt="w3layouts"></a>
		</div>
		<ul class="slide-list">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
		</ul>
	</div>
</div>
<div id="element"></div>
<!-- portfolio section -->
<section class="blog-w3ls">	
	<div class="container">
		<!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header text-center">博主推荐</h2>
                <ol class="breadcrumb">
                    <li><a href="index.jsp">主页</a>
                    </li>
                    <li class="active">博主</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
		<div class="row">
   <div class="col-md-4">
			<section class="blogwell-w3ls">
				<!-- Blog Search Well -->
				<div class="well">
					<h4>Blog Search</h4>
					<form action="#" method="post">
						<div class="input-group">
							<input type="text" class="form-control" id="search" placeholder="Search" required/>
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit" ><i class="fa fa-search"></i></button>
							</span>
						</div>
					</form>
					<!-- /.input-group -->
				</div>
				<!-- Blog Categories Well -->
				<div class="well">
					<h4>场合</h4>
					<div class="row">
						<div class="col-lg-12">
							<ul class="list-unstyled">
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=日常"><span class="fa fa-hand-o-right" aria-hidden="true"></span>日常<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=通勤"><span class="fa fa-hand-o-right" aria-hidden="true"></span>通勤<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=旅行"><span class="fa fa-hand-o-right" aria-hidden="true"></span>旅行<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
							</ul>
						</div>
						<!-- /.col-lg-6 -->
					</div>
					<!-- /.row -->
				</div>
                
                <div class="well">
					<h4>风格</h4>
					<div class="row">
						<div class="col-lg-12">
							<ul class="list-unstyled">
                                <li><a href="/fashion/ActionServlet?entity=Passage&method=selectStyle&style=淑女"><span class="fa fa-hand-o-right" aria-hidden="true"></span>淑女<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectStyle&style=OL"><span class="fa fa-hand-o-right" aria-hidden="true"></span>OL<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectStyle&style=秀场"><span class="fa fa-hand-o-right" aria-hidden="true"></span>秀场<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectStyle&style=运动"><span class="fa fa-hand-o-right" aria-hidden="true"></span>运动<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="well">
					<h4>身材</h4>
					<div class="row">
						<div class="col-lg-12">
							<ul class="list-unstyled">
                                <li><a href="/fashion/ActionServlet?entity=Passage&method=selectShape&shape=X型"><span class="fa fa-hand-o-right" aria-hidden="true"></span>X型<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectShape&shape=O型"><span class="fa fa-hand-o-right" aria-hidden="true"></span>O型<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectShape&shape=Y型"><span class="fa fa-hand-o-right" aria-hidden="true"></span>Y型<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="/fashion/ActionServlet?entity=Passage&method=selectShape&shape=H型"><span class="fa fa-hand-o-right" aria-hidden="true"></span>H型<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div> 
       </section>
            </div>
<!--            侧边栏结束-->
            <div class="col-md-8">
<!--                第一个人-->
                <div class="col-lg-6 col-md-4 col-sm-6 col-xs-6 port-agile">
				<div class="grid2">
					<figure class="effect-duke2">
						<img src="images/port-img3.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
                            <h3><font color="black">女神进化论</font></h3>
							<p class="port-w3ls">我们鼓励大家用科学的方法变美，是一本「内外兼修的成长教科书」
</p>
						</figcaption>			
					</figure>
				</div>
			</div>
<!--              第二个人  -->
            <div class="col-lg-6 col-md-4 col-sm-6 col-xs-6 port-agile">
				<div class="grid2">
					<figure class="effect-duke2">
						<img src="images/port-img4.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
                            <h3>Gogoboi</h3>
							<p class="port-w3ls">黑发黑眼不黑面、毒嘴毒舌不毒心；用专业辨认明星着装，用灵魂批判糟糕品味</p>
						</figcaption>			
					</figure>
				</div>
			</div> 
           
            <div class="col-lg-6 col-md-4 col-sm-6 col-xs-6 port-agile">
				<div class="grid2">
					<figure class="effect-duke2">
						<img src="images/port-img5.jpg" alt="img27" class="img-responsive"/> 
						<figcaption>
							<h3>热穿搭</h3>
							<p class="port-w3ls">只要你想要看的单品，这里都有！看完推荐，希望你再也不用纠结怎么穿啦！</p>
						</figcaption>			
					</figure>
				</div>
			</div>
			<div class="col-lg-6 col-md-4 col-sm-6 col-xs-6 port-agile">
				<div class="grid2">
					<figure class="effect-duke2">
						<img src="images/port-img6.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
							<h3>味姨</h3>
							<p class="port-w3ls">有品位，懂搭配，甩的链接都不贵。我都能想到最浪漫的事，就是和你一起变美！</p>
						</figcaption>			
					</figure>
				</div>
			</div>
            
            </div>
        </div>
    </div>
    </section>


<section class="footer0">
	<div class="container">
       <div class="container">
		<hr>
		<div class="copyright">
			<p>Copyright &copy; 2018.07  <a href="index.jsp">XFashion</a></p>
		</div>
	</div>
    </div>
    </section>
    
    
<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<!-- js for banner section -->
<!-- js for banner section -->
<script src="js/bliss-slider.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$("#slider").blissSlider({
			auto: 1,
      		transitionTime: 500,
      		timeBetweenSlides: 4000
		});
	});
</script>
<!-- /js for banner section -->
<!-- /js for banner section -->
<!-- /js files -->
</body>
</html>