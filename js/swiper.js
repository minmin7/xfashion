$(function(){
 var mySwiper = $('.banner').swiper({
    mode:'horizontal',
    loop: true,
    autoResize:true,
    pagination:'.banner .swiper-pagination',
    paginationClickable :true,
    autoplay : 3000,
 });

 var swiper = $('.case').swiper({
    pagination: '.case .swiper-pagination',
    slidesPerView: 4,
    slidesPerColumn: 2,
    paginationClickable: true,
    spaceBetween: 20
 });
})   