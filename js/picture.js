$(function(){
    //input的id;
 $("#upload").replaceWith('<input name="img" type="file" id="upload">');
    $("#upload").off('change').on('change',function () {
        var objURL=getObjectURL(this.files[0]);
        if (objURL){
            //图片的id;
            $("#avater").attr("src",objURL);
        }   
    });
    
    
});  

//建立一個可存取到該file的url
function getObjectURL(file) {
    var url = null ;
    if (window.createObjectURL!=undefined) { // basic
        url = window.createObjectURL(file) ;
    } else if (window.URL!=undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file) ;
    } else if (window.webkitURL!=undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file) ;
    }
    return url ;
}