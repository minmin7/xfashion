<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.fashion.entity.passageInfo"%>

<!DOCTYPE html>
<html>
<head>
<title>文章</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<!-- /fonts -->
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/trend.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/info.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/buttonstyle.css" rel="stylesheet" type="text/css" media="all" />   
<link href="css/modify.css" rel="stylesheet" type="text/css" media="all" /> 
<!-- /css -->
</head>
<body>
<!-- navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp"><h1>XFashion</h1></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp">主页</a></li>
				<li><a href="latest.jsp">最新</a></li>
				<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=通勤">分类</a></li>
				<li><a href="blogger.jsp">博主</a></li>
				<c:if test = "${empty sessionScope.user}">
                <li><a href="login.jsp">登录</a></li>
				</c:if>
				<c:if test = "${!empty sessionScope.user}">
                <li><a href="owner1.jsp">个人主页</a></li>
				</c:if>
			</ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<!-- /navigation -->
<!-- banner section -->
<div id="slider" class="slider-container2">
	<ul class="slider">
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner1.jpg" alt="An Image" draggable="false">
			</div>
		</li>
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner2.jpg" alt="An Image" draggable="false">
			</div>
		</li>
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner3.jpg" alt="An Image" draggable="false">
			</div>
		</li>
	</ul>
	<div class="slider-controls">
		<div class="slide-nav">
			<a href="#" class="prev"><img src="images/prev.png" alt="w3layouts"></a>
			<a href="#" class="next"><img src="images/next.png" alt="w3layouts"></a>
		</div>
		<ul class="slide-list">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
		</ul>
	</div>
</div>
<div id="element"></div>
<!-- /banner section -->
<section class="blog-post">
        <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
    
            
                <h2 class="page-header text-center">连衣裙+运动鞋，才是夏天最美的套路 
                    <small>by <a href="#">热搭配</a>
                    </small>
                </h2>
                <ol class="breadcrumb">
                    <li><a href="index.jsp">主页</a>
                    </li>
					<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=日常">分类</a></li>
                    <li class="active">文章</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->

                <hr>
				<!-- <div class="hover01 column">
				 -->				
		    <hr>

            
                <p class="lead">
                         高跟鞋配连衣裙似乎是众所周知，但是对于穿不习惯的小可爱们来说，高跟鞋简直就是噩梦，特别是逛街的时候真的是超级累，小搭真的是受不住的。

现在就不用怕了，有种鞋子穿起来不仅舒适，搭配上连衣裙就是美美美！好吧，相信大家差不都都知道了，没错，就是运动鞋，不管是搭什么裙子，完全能hold住呢，既舒适又可以时髦。

在各大时装的大秀上，裙子搭上运动鞋也是十分的受欢迎的，今年的LV时装秀上就展示了连衣裙和运动鞋的无限魅力。
        </p>
  <img class="img-responsive" src="images/rdp1.jpg">
  <hr>
  <p class="blogpost-agile">
                不仅是在秀场上，就连各大时尚博主、明星也争先恐后的走上了这场时尚比美。大表姐可以说是行走的衣架，一件吊带裙搭上一双运动鞋，简单舒适，还很时髦。
    </p>
<img class="img-responsive" src="images/rdp2.jpg">
 <p class="blogpost-agile">看了这么多，小可爱们也是可以拥有的，很简单的，想逛街一天不累，赶紧翻出鞋柜里的那双运动鞋，也能够穿出多重的风格。 </p>
                

<hr>
<p class="blogpost-agile"></p>
               
				<hr>
            
                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form action="#" method="post" role="form">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" id="comments" name="comments"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->
.
                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
						<div class="hover01 column">
							<div>
								<figure><img class="media-object" src="images/blogpost1.jpg" alt=""></figure>
							</div>
						</div>
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">居老师的大可爱孟孟今天吸居了吗
                            
                        </h4>
                        <p class="blogpost-agileits">小姐姐好好看啊啊啊啊啊 土拨鼠尖叫 </p>
                    </div>
                </div>

                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <div class="hover01 column">
							<div>
								<figure><img class="media-object" src="images/blogpost2.jpg" alt=""></figure>
							</div>
						</div>
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">一只胖兔子
                            
                        </h4>
                        <p class="blogpost-agileits">沙发！</p>
                        <!-- Nested Comment -->
                        <div class="media">
                            <a class="pull-left" href="#">
								<div class="hover01 column">
									<div>
										<figure><img class="media-object" src="images/blogpost3.jpg" alt=""></figure>
									</div>
								</div>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">敏感的阿司匹林
                                   
                                </h4>
                                <p class="blogpost-agileits">你晚了哈哈~</p>
                            </div>
                        </div>
                        <!-- End Nested Comment -->
                    </div>
                </div>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4 blog-post-w3layouts">
				<!-- Blog Search Well -->
				<div class="well blog-post-agileits">
					<h4 class="blogpost-w3lsagile">文章搜索</h4>
					<form action="#" method="post">
						<div class="input-group">
							<input type="text" class="form-control" name="search" id="search" placeholder="Search" required/>
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit" ><i class="fa fa-search"></i></button>
							</span>
						</div>
					</form>
					<!-- /.input-group -->
				</div>
				<!-- Side Widget Well -->
<!--
				<div class="well blog-post-agileits">
					<h4>作者</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
				</div>	
-->
                <!-- Blog Categories Well -->
				<div class="well blog-post-agileits">
					<h4 class="blogpost-w3lsagile">他的文章</h4>
					<div class="row">
						<div class="col-lg-12">
							<ul class="list-unstyled">
								<li><a href="#"><span class="fa fa-hand-o-right" aria-hidden="true"></span>标题1<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="#"><span class="fa fa-hand-o-right" aria-hidden="true"></span>标题2<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
								<li><a href="#"><span class="fa fa-hand-o-right" aria-hidden="true"></span>Lorem Ipsum Dolor<span class="fa fa-hand-o-left" aria-hidden="true"></span></a></li>
							</ul>
						</div>
					</div>
					</div>
					<!-- /.row -->
				</div>
				
            </div>
        
        
        <!-- /.row -->
        <div class="likesection">
        
        <c:if test="${likeflag!=1 }">
            <button type="button" class="like" id="likepassage" onclick="like()"></button>
        </c:if>
        <c:if test="${likeflag==1 }">
            <button type="button" class="liked" id="likedpassage" onclick="like()"></button>
        </c:if>
        </div>
        <hr>
        
        
    </div>
    <!-- /.container -->
    
</section>	

    
    
    <!-- footer section -->

<!-- footer section -->	
<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<!-- js for banner section -->
<script src="js/bliss-slider.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		
		
		$("#slider").blissSlider({
			auto: 1,
      		transitionTime: 500,
      		timeBetweenSlides: 4000
		});
		
		$("#pppp").load("rdp.html"); 
		
	});
    
$(".like").click(function(){
    $("#likepassage").show();
});


</script>

<!-- /js for like section -->



<!-- /js files -->
</body>
</html>