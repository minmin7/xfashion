<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>最新资讯</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<!-- /fonts -->
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/trend.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/info.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- /css -->
</head>
<body>
<!-- navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp"><h1>XFashion</h1></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp">主页</a></li>
				<li class="active"><a href="latest.jsp">最新</a></li>
				<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=通勤">分类</a></li>
				<li><a href="blogger.jsp">博主</a></li>
				<c:if test = "${empty sessionScope.user}">
                <li><a href="login.jsp">登录</a></li>
				</c:if>
				<c:if test = "${!empty sessionScope.user}">
                <li><a href="/fashion/ActionServlet?entity=Owner&method=selectByName">个人主页</a></li>
				</c:if>
			</ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<!-- /navigation -->
<!-- banner section -->
<div id="slider" class="slider-container2">
	<ul class="slider">
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner1.jpg" alt="An Image" draggable="false">
			</div>
		</li>
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner2.jpg" alt="An Image" draggable="false">
			</div>
		</li>
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner3.jpg" alt="An Image" draggable="false">
			</div>
		</li>
	</ul>
	<div class="slider-controls">
		<div class="slide-nav">
			<a href="#" class="prev"><img src="images/prev.png" alt="w3layouts"></a>
			<a href="#" class="next"><img src="images/next.png" alt="w3layouts"></a>
		</div>
		<ul class="slide-list">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
		</ul>
	</div>
</div>
<div id="element"></div>
<!-- /banner section -->   
<!-- Page Content -->
<div class="container">
<!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header text-center">最新搭配图片</h2>
            <ol class="breadcrumb">
                <li><a href="index.jsp">主页</a></li>
                <li class="active">最新</li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

	<!-- latest -->
<section class="latest">	
    <div class="row">
<!--
        <div class="col-lg-12">
            <h3 class="page-header text-center">最新搭配图片</h3>
        </div>
-->
      <div class="col-md-4 col-sm-6 text-center">
            <div class="thumbnail">
               <div class="hover01 column">
                <div id="myCarousel1" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel1"  data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel1"  data-slide-to="1"></li>
                            <li data-target="#myCarousel1"  data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active" >
                                <img src="images/latest1-1.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img src="images/latest1-2.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img src="images/latest1-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div> 
                </div>
        </div>

        <div class="col-md-4 col-sm-6 text-center" >
            <div class="thumbnail">
                <div class="hover01 column">
                <div id="myCarousel2" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel2" data-slide-to="1"></li>
                            <li data-target="#myCarousel2" data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="images/latest2-1.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img src="images/latest2-2.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img src="images/latest2-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div>
            </div>
        </div>  
     
	 <!-- latest3 -->	
       <div class="col-md-4 col-sm-6 text-center">
            <div class="thumbnail">
                <div class="hover01 column">
                <div id="myCarousel3" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel3" data-slide-to="1"></li>
                            <li data-target="#myCarousel3" data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active" style="margin-top: 0 auto">
                                <img src="images/latest3-1.jpg" alt="First slide">
                            </div>
                            <div class="item" style="height=400px;borde solid 1px;">
                                <img src="images/latest3-2.jpg" alt="Second slide" >
                            </div>
                            <div class="item" >
                                <img src="images/latest3-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel3" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel3" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div>
            </div>
        </div>
       
      <div class="col-md-4 col-sm-6 text-center" >
            <div class="thumbnail">
                <div class="hover01 column">
                <div id="myCarousel4" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel4" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel4" data-slide-to="1"></li>
                            <li data-target="#myCarousel4" data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="images/latest4-1.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img src="images/latest4-2.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img src="images/latest4-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel4" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel4" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div>
            </div>
        </div>  
        <div class="col-md-4 col-sm-6 text-center" >
            <div class="thumbnail">
                <div class="hover01 column">
                <div id="myCarousel5" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel5" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel5" data-slide-to="1"></li>
                            <li data-target="#myCarousel5" data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="images/latest5-1.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img src="images/latest5-2.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img src="images/latest5-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel5" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel5" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div>
            </div>
        </div>     
         <!-- latest6 -->
        <div class="col-md-4 col-sm-6 text-center" >
            <div class="thumbnail">
                <div class="hover01 column">
                <div id="myCarousel6" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel6" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel6" data-slide-to="1"></li>
                            <li data-target="#myCarousel6" data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="images/latest6-1.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img src="images/latest6-2.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img src="images/latest6-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel6" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel6" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div>
            </div>
        </div>  
          <!-- latest7 -->
        <div class="col-md-4 col-sm-6 text-center" >
            <div class="thumbnail">
                <div class="hover01 column">
                <div id="myCarousel7" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel7" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel7" data-slide-to="1"></li>
                            <li data-target="#myCarousel7" data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="images/latest7-1.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img src="images/latest7-2.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img src="images/latest7-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel7" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel7" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div>
            </div>
        </div> 
          <!-- latest8 -->
        <div class="col-md-4 col-sm-6 text-center" >
            <div class="thumbnail">
                <div class="hover01 column">
                <div id="myCarousel8" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel8" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel8" data-slide-to="1"></li>
                            <li data-target="#myCarousel8" data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="images/latest8-1.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img src="images/latest8-2.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img src="images/latest8-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel8" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel8" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div>
            </div>
        </div> 
        
        <div class="col-md-4 col-sm-6 text-center" >
            <div class="thumbnail">
                <div class="hover01 column">
                <div id="myCarousel9" class="carousel slide" data-interval="false">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel9" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel9" data-slide-to="1"></li>
                            <li data-target="#myCarousel9" data-slide-to="2"></li>
                        </ol>   
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="images/latest9-1.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img src="images/latest9-2.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img src="images/latest9-3.jpg" alt="Third slide">
                            </div>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel9" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel9" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> 
                </div>
            </div>
        </div> 
        
    </div>
        <!-- /.row -->
</section>
    </div>

   
<!-- footer section -->
    <section class="footer0">
	<div class="container">
       <div class="container">
		<hr>
		<div class="copyright">
			<p>Copyright &copy; 2018.07  <a href="index.html">XFashion</a></p>
		</div>
	</div>
    </div>
    </section>
<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<!-- js for banner section -->
<!-- js for banner section -->
<script src="js/bliss-slider.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$("#slider").blissSlider({
			auto: 1,
      		transitionTime: 500,
      		timeBetweenSlides: 4000
		});
	});
</script>
<!-- /js for banner section -->
<!-- /js for banner section -->
<!-- /js files -->
</body>
</html>