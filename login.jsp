<%@page language="java" contentType="text/html;charset=UTF-8" pageEncoding = "UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<!-- /fonts -->
<!-- include all css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/typo.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/trend.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/info.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- /css -->
</head>
<body > 
<!--<section class="loginback"></section>-->
    <!-- navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp"><h1>XFashion</h1></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp">主页</a></li>
				<li><a href="latest.jsp">最新</a></li>
				<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=通勤">分类</a></li>
				<li><a href="blogger.jsp">博主</a></li>
				<li class="active"><a href="login.jsp">登录</a></li>
<!--               <li><a onclick="window.open('login.html')">登录</a></li>-->
<!--				<li><a href="contact.html">我的</a></li>-->
			</ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>


  <section class="singlebackground">

	<div class="main">

				<h1><font color="white">XFashion</font></h1>
				<hr>
				<div class="footer-subsc-w3ls">
					<form id="reg" method="post" class="form-horizontal" role="form">
				<!--  	<form id="reg" method="post" action="http://localhost:8080/fashion/ActionServlet" class="form-horizontal" role="form">
                       <div>
                            <input hidden="true" name="entity" value="User"/>
                            <input hidden="true" name="method" value="checkLogin"/>
                        </div>
                 -->
						<div class="form-group">
							<div class="col-lg-12">
								<input type="text" class="form-control" name="username"  placeholder="用户名" value="" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-12">
								<input type="password" class="form-control" name="password" placeholder="密码" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-12">
								<button type="button" class="btn-outline" onclick="login()">登录</button>	
                            <a href="register.jsp">
                                <button type="button" class="btn-outline" onclick="window.location.href='register.jsp'">立即注册</button>
                                </a>
                            </div>
						</div>
					</form> 
				</div>
			</div>
				
		

</section>

    
    
    
   
<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<!-- js for banner section -->
<script src="js/bliss-slider.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$("#slider").blissSlider({
			auto: 1,
      		transitionTime: 500,
      		timeBetweenSlides: 4000
		});
	});
	
</script>

<script>
		function login(){
			$.ajax({
                type: "POST",//方法类型
                url: "/fashion/ActionServlet?entity=User&method=checkLogin" ,//url
                data: $('#reg').serialize(),
                success: function (result) {
                 //   console.log(result);//打印服务端返回的数据(调试用)
                    if (result == "success") {
                        alert("登录成功");
                        window.location.href = "/fashion/index.jsp";
                    }
                    else if(result == "fail")
                    {
                    	alert("用户名与密码不匹配");
                    }
                },
                error : function() {
                    alert("登录失败");
                }
            });
		}
	</script>


<!-- /js for banner section -->
<!-- /js files -->
</body>
</html>
