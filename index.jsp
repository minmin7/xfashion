<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>XFashion</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<!-- /fonts -->
<!-- include all css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/typo.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/trend.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/info.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- /css -->
</head>
<body>
<!-- navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp"><h1>XFashion</h1></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="index.jsp">主页</a></li>
				<li><a href="latest.jsp">最新</a></li>
				<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=通勤">分类</a></li>
				<li><a href="blogger.jsp">博主</a></li>
				<c:if test = "${empty sessionScope.user}">
                <li><a href="login.jsp">登录</a></li>
				</c:if>
				<c:if test = "${!empty sessionScope.user}">
                <li><a href="/fashion/ActionServlet?entity=Owner&method=selectByName">个人主页</a></li>
                <li><a href="#"></a></li>
				</c:if>
				
			</ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<!-- /navigation -->
<!-- banner section -->
<div id="slider" class="slider-container">
	<ul class="slider">
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner1.jpg" alt="An Image" draggable="false">
			</div>
			<div class="slide-content">
				<h3>We Bring Fashion To Life</h3>
				<p>我们展示时尚，而你定义时尚</p>
				<a href="latest.jsp">Read More</a>
			</div>
		</li>
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner2.jpg" alt="An Image" draggable="false">
			</div>
			<div class="slide-content">
				<h3>The Power Of Fashion</h3>
				<p>时尚，一种将美丽和自信放至最大的力量</p>
				<a href="latest.jsp">Read More</a>
			</div>
		</li>
		<li class="slide">
			<div class="slide-bg">
				<img src="images/banner3.jpg" alt="An Image" draggable="false">
			</div>
			<div class="slide-content">
				<h3>Fashion, See More, Do More</h3>
				<p>不追赶时尚，而是拥有自己的时尚</p>
				<a href="latest.jsp">Read More</a>
			</div>
		</li>
	</ul>
	<div class="slider-controls">
		<div class="slide-nav">
			<a href="#" class="prev"><img src="images/prev.png" alt="w3layouts"></a>
			<a href="#" class="next"><img src="images/next.png" alt="w3layouts"></a>
		</div>
		<ul class="slide-list">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
		</ul>
	</div>
</div>
<div id="element"></div>
<!-- /banner section -->
<!-- info section -->
<section class="info">
	<h2 class="text-center">分类查看</h2>
	<p class="text-center">选择您感兴趣的场合、风格、或适合您身材的穿搭分享</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 info-w3l">
				<ul class="ch-grid">
					<li>
						<div class="ch-item ch-img-1">				
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front ch-img-1"></div>
									<div class="ch-info-back1">
										<a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=日常"><h4>场合</h4></a>
										<p class="info-w3lsagile">Occasion</p>
									</div>	
								</div>
							</div>
						</div>
					</li>
				</ul>	
				<h3>场合</h3>
				<p class="info-agile">尽情挑选适合您不同场合搭配的衣服，无论是通勤、出游还是约会，我们应有尽有，各位小主前来宠幸吧！</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 info-w3l">
				<ul class="ch-grid">
					<li>
						<div class="ch-item ch-img-2">				
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front ch-img-2"></div>
									<div class="ch-info-back2">
										<a href="/fashion/ActionServlet?entity=Passage&method=selectStyle&style=淑女"><h4>风格</h4></a>
										<p class="info-w3lsagile">Style</p>
									</div>	
								</div>
							</div>
						</div>
					</li>
				</ul>
				<h3>风格</h3>
				<p class="info-agile">挑选您想要尝试的各种风格的服饰搭配，无论是森女、洛丽塔还是时髦精，我们已经为您搭配好了，赶紧来尝试一下吧！</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 info-w3l info3">
				<ul class="ch-grid">
					<li>
						<div class="ch-item ch-img-3">				
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front ch-img-3"></div>
									<div class="ch-info-back3">
										<a href="/fashion/ActionServlet?entity=Passage&method=selectShape&shape=X型"><h4>身材</h4></a>
										<p class="info-w3lsagile" >Body Shape</p>
									</div>	
								</div>
							</div>
						</div>
					</li>
				</ul>
				<h3>身材</h3>
				<p class="info-agile">找到最合适您身材的搭配小技巧，无论是娇小妹子长裙搭配还是丰满妹子超显瘦搭配，这里已经为您准备好了，不来看看嘛~</p>
			</div>
		</div>
	</div>
</section>
<!-- /info section -->
<!-- staff-info section -->
<section class="staff-info">
	<h3 class="text-center">热门文章</h3>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 staff-w3l1 height=100px">
				<div class="staff-details">
					<div class="staff-agile1">
						<!--<img src="images/staff-img1.jpg" alt="w3layouts" class="img-circle img-responsive">-->
				        <div class="staff-w3lagile">
							<h3>你知道有一种裤子叫“初恋裤”吗？穿好了可以变成他初恋的样子哦～</h3>
						</div>
                        <div class="staff-w4lagile">
                            <p>Gogoboi</p>
                        </div>
						<div class="clearfix"></div>
					</div>
					<div class="staff-wthree" style="overflow-y: auto">
						<p>
                        我是文艺女青年请和我谈恋爱背带裤搭衬衫<br>
                        我是有点小性感的少女，请和我谈恋爱背带裤搭一字领上衣<br>
                        我那么有个性，请和我谈恋爱背带裤搭露腰上衣<br>
                        我很乖，请和我谈恋爱背带裤搭T恤
                        </p>
<!--                        https://mp.weixin.qq.com/s/3UTtPD08XT7vllJkOgW4cg-->
					</div>
					<div class=gotopa>
					<a href="ggb.jsp">阅读全文</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 staff-w3l2">
				<div class="staff-details">
					<div class="staff-agile1">
						<!--<img src="images/staff-img2.jpg" alt="w3layouts" class="img-circle img-responsive">-->
						<div class="staff-w3lagile">
							<h3>怎样穿出迷人的高级感？这5个搭配雷区千！万！别！踩！</h3>
						</div>
                        <div class="staff-w4lagile">
                          <p>女神进化论</p>  
                        </div>
                        
						<div class="clearfix"></div>
					</div>
					<div class="staff-wthree" style="overflow-y: auto">
						<p>很多同学都很关心如何穿出「高级感」，但因为高级感是个很大的话题，涉及到单品的材质、版型、搭配等等，所以今天只想给大家先聊聊「高级感的配色」。</p>
                        
					</div>
<!--                    https://mp.weixin.qq.com/s/pSPPymDqAK2KiH5xRdV6lQ-->
				<div class=gotopa>
					<a href="jhl.jsp">阅读全文</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 staff-w3l3">
				<div class="staff-details">
					<div class="staff-agile1">
<!--						<img src="images/staff-img3.jpg" alt="w3layouts" class="img-circle img-responsive">-->
						<div class="staff-w3lagile">
							<h3>连衣裙+运动鞋，才是夏天最美的套路！</h3>
						</div>
                        <div class="staff-w4lagile">
                           <p>热搭配</p>
                        </div>
						<div class="clearfix"></div>
					</div>
					<div class="staff-wthree" style="overflow-y: auto">
						<p>高跟鞋配连衣裙似乎是众所周知，但是对于穿不习惯的小可爱们来说，高跟鞋简直就是噩梦，特别是逛街的时候真的是超级累，小搭真的是受不住的。运动鞋穿起来不仅舒适，搭配上连衣裙更是美美美！不管是搭什么裙子，完全能hold住，既舒适又时髦。</p>
					</div>
					<div class=gotopa>
					<a href="rdp.jsp">阅读全文</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

    
<!-- /testimonial section -->
<!-- trends section -->
<section class="trend">
    <h3 class="text-center"><font color="black">Check Out The Latest Trends</font></h3>
	<p class="text-center"><font color="black">来看看明星们的日常私服穿搭吧</font></p>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 trend-w3layouts">
				<div class="view view-seventh">
                    <img src="images/trend-img1.jpg" alt="w3layouts" class="img-responsive"/>
                    <div class="mask">
                        <h4>李沁</h4>
                        <p>身着经典有型的格纹西装现身机场,搭配舒适简约的T恤,牛仔裙,帅气时髦,大秀一双修长美腿。</p>
                        <a href="latest.jsp" class="info">Read More</a>
                    </div>
				</div>
                <h5><font color="black">李沁</font></h5>
                <p class="trend-w3ls"><font color="black">
                    Paul Smith格纹西装<br>
                    AllSaint牛仔短裙
                    </font></p>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 trend-w3layouts">
				<div class="view view-seventh">
                    <img src="images/trend-img2.jpg" alt="w3layouts" class="img-responsive"/>
                    <div class="mask">
                        <h4>朱一龙</h4>
                        <p>身着简约的sloganT和随性的oversize衬衫现身机场，简直是一枚行走的衣架子</p>
                        <a href="latest.jsp" class="info">Find More</a>
                    </div>
				</div>
                <h5><font color="black">朱一龙</font></h5>
                <p class="trend-w3ls"><font color="black">BalenciagaT恤<br>Balenciaga衬衫</font></p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 trend-w3layouts">
				<div class="view view-seventh">
                    <img src="images/trend-img3.jpg" alt="w3layouts" class="img-responsive"/>
                    <div class="mask">
                        <h4>佟丽娅</h4>
                        <p>笑容甜美的丫丫一身条纹休闲套装清新亮丽，斜挎灰蓝拼色斜挎包，别致有趣，吸睛十足。</p>
                        <a href="latest.jsp" class="info">Find More</a>
                    </div>
				</div>
                <h5><font color="black">佟丽娅</font></h5>
                <p class="trend-w3ls"><font color="black">Mother套装<br>Valextra Iside斜挎包</font></p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 trend-w3layouts">
				<div class="view view-seventh">
                    <img src="images/trend-img4.jpg" alt="w3layouts" class="img-responsive"/>
                    <div class="mask">
                        <h4>鹿晗</h4>
                        <p><font>清新的白色帽衫配上帅气的黑色字母飞行夹克帅气满分，反戴的帽子少年感满满</font></p>
                        <a href="latest.jsp" class="info">Find More</a>
                    </div>
				</div>
                <h5><font color="black">鹿晗</font></h5>
                <p class="trend-w3ls"><font color="black">OFF White连帽卫衣<br>Louis Vuitton夹克</font></p>
			</div>
		</div>
	</div>
</section>
<!-- trends section -->
<!-- portfolio section -->
<section class="portfolio">
	<h3 class="text-center">Check Out The Best Blogger</h3>
	<p class="text-center">为您推荐最in的时尚达人</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="grid1" onclick="window.open('xv.jsp');">
					<figure class="effect-duke1">
						<img src="images/port-img1.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
							<h3>Xavier杰斯君</h3>
							<p class="port-w3ls">时髦，就是来自骨子里的，服装是它的扬声器，让时髦更动听。</p>
						</figcaption>			
					</figure>
				</div>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 port-agile">
				<div class="grid2" onclick="window.open('#');">
					<figure class="effect-duke2">
						<img src="images/port-img3.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
							<h3>女神进化论</h3>
							<p class="port-w3ls">我们鼓励大家用科学的方法变美，是一本「内外兼修的成长教科书」
</p>
						</figcaption>			
					</figure>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 port-agile">
				<div class="grid2" onclick="window.open('owner1.jsp');">
					<figure class="effect-duke2">
						<img src="images/port-img4.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
                            <h3>Gogoboi</h3>
							<p class="port-w3ls">黑发黑眼不黑面、毒嘴毒舌不毒心；用专业辨认明星着装，用灵魂批判糟糕品味</p>
						</figcaption>			
					</figure>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="grid1" onclick="window.open('#');">
					<figure class="effect-duke1">
						<img src="images/port-img2.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
							<h3>haruru</h3>
							<p class="port-w3ls">时尚达人，不自然なBOY，微博@haruru，以及，我是男的   </p>
						</figcaption>			
					</figure>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 port-agile">
				<div class="grid2" onclick="window.open('#');">
					<figure class="effect-duke2">
						<img src="images/port-img5.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
							<h3>热穿搭</h3>
							<p class="port-w3ls">只要你想要看的单品，这里都有！看完推荐，希望你再也不用纠结怎么穿啦！</p>
						</figcaption>			
					</figure>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 port-agile">
				<div class="grid2" onclick="window.open('#');">
					<figure class="effect-duke2">
						<img src="images/port-img6.jpg" alt="img27" class="img-responsive"/>
						<figcaption>
							<h3>味姨</h3>
							<p class="port-w3ls">有品位，懂搭配，甩的链接都不贵。我都能想到最浪漫的事，就是和你一起变美！</p>
						</figcaption>			
					</figure>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- portfolio section -->

<!-- footer section -->
<section class="footer0">
	<div class="container">
       <div class="container">
		<hr>
		<div class="copyright">
			<p>Copyright &copy; 2018.07  <a href="index.html">XFashion</a></p>
		</div>
	</div>
    </div>
    </section>
<!-- footer section -->

<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<!-- js for banner section -->
<script src="js/bliss-slider.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$("#slider").blissSlider({
			auto: 1,
      		transitionTime: 500,
      		timeBetweenSlides: 4000
		});
	});
</script>
<!-- /js for banner section -->
<!-- /js files -->
</body>
</html>