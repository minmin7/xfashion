<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.fashion.entity.passageInfo"%>
<%@ page language ="java" import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<!-- /fonts -->
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/typo.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/trend.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/info.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/buttonstyle.css" rel="stylesheet" type="text/css" media="all" />   
<link href="css/modify.css" rel="stylesheet" type="text/css" media="all" /> 
<script src="js/picture.js" type="text/javascript"></script>
<!-- /css -->
</head>
<body>
<!-- navigation  导航栏-->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.html"><h1>XFashion</h1></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li ><a href="index.jsp">主页</a></li>
				<li><a href="latest.jsp">最新</a></li>
				<li><a href="/fashion/ActionServlet?entity=Passage&method=selectOccasion&occasion=通勤">分类</a></li>
				<li><a href="blogger.jsp">博主</a></li>
<!--				<li><a href="typo.html">Typography</a></li>-->
				<li class = "active"><a href="/fashion/ActionServlet?entity=Owner&method=selectByName">个人主页</a></li>
			  <li><a href="/fashion/ActionServlet?entity=User&method=logout"  id="" >logout</a></li>
			</ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<!-- /navigation -->

<!-- staff-info section -->
<section class="staff-info1">
    

	<div class="container">
			<div class="text-center">
				<img src="images/owner-go.jpg" alt="w3layouts" class="img-circle img-responsive"  id = "avater"   style="margin:0 auto" type="">
            <a class = "a-upload" >
                <input type="file" id="upload" >
             </a>

            <div class="staff-wthree1">
               <h3  style = "color:black">Gogoboi</h3>
                 </div>
                 
		    	
			
                 <input type = "button" class = "file modify-1"  value = "修改昵称">
                <div class="clearfix"></div>

					<div class="staff-wthree">
						<p>黑发黑眼不黑面、毒嘴毒舌不毒心；用专业辨认明星着装，用灵魂批判糟糕品味</p>
                        <input type = "button" class = "file modify-2"  value = "修改个人签名">
                        
<!--                       <input type = "button" class = "file cancel-editor"  value = "取消">    -->
                </div>
    </div> 
				</div>
    
    <!--一个弹窗！！！！！！-->
    
    <form method="post" action="">
        <div class = "modify" id ="change-nickname">
            <table width = "100%" border = "0">
            <caption>修改昵称</caption>
            <tr><td><textarea class="sss" name = "nickname" size = "30"></textarea></td></tr>
            <tr>
                <td colspan="2" align = "center">
                <button type="submit" class="file">保存</button>
                <input type = "button" class = "file cancel-editor" value="取消">
                </td>
                </tr>
            </table>      
        </div>
    </form>
       
     <form method="post" action="">
    <div id ="change-persontitle"  class = "modify" >
        <table width= "100%" border = "0">
        <caption>修改个人资料</caption>

        <tr><td><textarea class="sss" name = "nickname" size = "30"></textarea></td></tr>
        <tr>
            <td colspan="2" align = "center">
            <button type = "submit" class="file">保存</button>
            <input type = "button" class = "file cancel-editor" value="取消">
            </td>
            </tr>
        </table>      
    </div>
    </form>
    
    
    
<!--<div id = "note-frame" >
        <form action="" method="post" enctype="multipart/form-data">
            <div class = "new-frame ">
            <input id= "note-id" name = "ID" type = "hidden" value = "1111"/>
            <input id = "note-title" name = "title" value = "标题"/>
                <textarea id = "modify-introduction" name  = "introduction" rows="2"></textarea>
               <input name = "img" id = "personsImage" type="file"  value = "介绍" style="margin-top: 0px;" onchange="uploadImg(this)"/>
                <button type = "submit">确定</button>
                <button type = "button">取消</button>                 
            </div>
        </form>             
    </div>-->

</section>
<!-- /staff-info section -->
    <div class="container">
 <section class="service-tabs">
        <!-- Service Tabs -->	
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-center">个人主页</h3>
            </div>
            <div class="col-lg-12">

                <ul id="myTab" class="nav nav-tabs nav-justified">
                    <li class="active"><a href="#service-one" data-toggle="tab"><i class="fa fa-camera-retro"></i> 收藏文章</a>
                    </li>
                    <li class=" "><a href="#service-two" data-toggle="tab"><i class="fa fa-binoculars"></i>原创文章</a>
                    </li>
                    <li class=" "><a href="#service-three" data-toggle="tab"><i class="fa fa-desktop"></i>写篇文章</a>
                    </li>
                </ul>
                
  
                <div id="myTabContent" class="tab-content">
                    <!-- 第一部分收藏文章-->
                    <div class="tab-pane fade active in" id="service-one">
                        <section class="contact-us">
                         <h3 class="contact-agileits2">收藏文章</h3>  
               
			<!-- First Blog Post -->
            <h3><a href="blog-post.html">Lorem Ipsum Dolor</a></h3>
            <p class="lead">by <a href="index.html">Vanity Fair</a></p>
            <p class="blog-agile1"><i class="fa fa-clock-o"></i> Posted on May 28, 2016 at 10:00 PM</p>
            <hr>
            <a href="blog-post.html">
				<div class="hover01 column">
					<div>
						<figure><img class="img-responsive img-hover" src="images/blog-img1.jpg" alt=""></figure>
					</div>
				</div>
            </a>
            <hr>
            <p class="blog-agile2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
            <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
			<hr>
			
			<!-- Second Blog Post -->
            <h3><a href="blog-post.html">Lorem Ipsum Dolor</a></h3>
            <p class="lead">by <a href="index.php">Vanity Fair</a></p>
            <p class="blog-agile1"><i class="fa fa-clock-o"></i> Posted on May 29, 2016 at 10:45 PM</p>
            <hr>
            <a href="blog-post.html">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive img-hover" src="images/blog-img2.jpg" alt=""></figure>
					</div>
				</div>
            </a>
            <hr>
            <p class="blog-agile2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, quasi, fugiat, asperiores harum voluptatum tenetur a possimus nesciunt quod accusamus saepe tempora ipsam distinctio minima dolorum perferendis labore impedit voluptates!</p>
            <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
			<hr>
			<!-- Third Blog Post -->
            
			<h3><a href="blog-post.html">Lorem Ipsum Dolor</a></h3>
            <p class="lead">by <a href="index.php">Vanity Fair</a></p>
            <p class="blog-agile1"><i class="fa fa-clock-o"></i> Posted on August 30, 2016 at 10:45 PM</p>
            <hr>
            <a href="blog-post.html">
				<div class="hover01 column">
					<div>
						<figure><img class="img-responsive img-hover" src="images/blog-img3.jpg" alt=""></figure>
					</div>
				</div>
            </a>
            <hr>
            <p class="blog-agile2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, voluptates, voluptas dolore ipsam cumque quam veniam accusantium laudantium adipisci architecto itaque dicta aperiam maiores provident id incidunt autem. Magni, ratione.</p>
            <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
			<hr>

            <!-- Pager -->
<!--
            <ul class="pager">
                <li class="previous">
                    <a href="#">&larr; Older</a>
                </li>
                <li class="next">
                    <a href="#">Newer &rarr;</a>
                </li>
            </ul>
-->
		
                      </section>   
                    </div>
                   
                
                
                <!--第二部分原创文章-->

                    <div class="tab-pane fade" id="service-two">
                        <section class="contact-us">
                        <h3 class="contact-agileits2">原创文章</h3>
                     
			<% List<passageInfo> l2 = (List <passageInfo>)request.getAttribute("lina"); %>
       		<% for(passageInfo p:l2)%><%{ 
       		%>
       		    		
            <h3><%=p.getTitle() %></h3>
            <p class="lead">by <a href="owner1.jsp"><%=p.getUname()%></a></p>
            
            <p class="blog-agile1"><%=p.getCreatTime()%></p>
            <hr>
            <p class="blog-agile2"><%=p.getSimplecontent() %></p>
            
            <a class="btn btn-primary"><%=p.getDetail() %><i class="fa fa-angle-right"></i></a>
			<hr>
			<%}%>
                  </section>      
                    </div>                
                <!--第三部分自己提交文章--->
    <div class="tab-pane fade" id="service-three">
<!--                    <h4>写篇文章</h4>-->
        <section class="contact-us">
            <div class = "row">
                <form method="post" id = "con"  enctype="multipart/form-data">    
                 <h3 class="contact-agileits2">写篇文章</h3>
                    <div class="control-group form-group">
                        <div class="controls">
							<label>文章标题:</label>	
                            <input type="text" class="form-control" name="title" id="title" placeholder="请输入文章标题，字数少于二十个字" required/>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>文章简介:</label>
                            <input type="tel" class="form-control" name="simplecontent" id="simplecontent" placeholder="请输入一段文章简介，对内容进行一个概述" required/>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <p>请为您的文章选择一个标签</p>
                        <div class="controls">
                            <label>场合:</label>
                            <input type="radio" name="occasion"   value="日常" />
                        <label for="title1">日常</label>
                           <input type="radio" name="occasion"   value="通勤" />
                             <label for="title2">通勤</label>  
                           <input type="radio" name="occasion"    value="出游" />
                             <label for="title2">出游</label>  
                        </div>
                        <div class="controls">
                            <label>风格:</label>
                            <input type="radio" name="style"  value="淑女" />
                        <label for="title1">淑女</label>
                           <input type="radio" name="style"  value="OL" />
                             <label for="title2">OL</label>  
                           <input type="radio" name="style" value="秀场" />
                             <label for="title2">秀场</label> 
                           <input type="radio" name="style"  value="运动" />
                             <label for="title2">运动</label>
                        </div> 
                         <div class="controls">
                           <label>身材:</label>
                            <input type="radio" name="shape"  value="X型" />
                        <label for="title1">X型</label>
                           <input type="radio" name="shape"  value="O型" />
                             <label for="title2">O型</label>  
                           <input type="radio" name="shape"  value="Y型" />
                             <label for="title2">Y型</label> 
                           <input type="radio" name="shape"  value="H型" />
                             <label for="title2">H型</label>
                        </div> 
                        
                            
                    </div>
                    <input type="hidden" id="content" name="text" value=""/>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>文章上传：</label>
                             <div id="diveditor">
                              <!-- <p>欢迎使用 <b>XFashion</b>编辑器</p>--> 
                            </div>
                        </div>
                    </div>
                <script type="text/javascript" src="./js/wangEditor.min.js"></script>
                <script>
                    var E = window.wangEditor
                    var editor = new E('#diveditor')
                editor.customConfig.uploadImgShowBase64 = true   // 使用 base64 保存图片
                      editor.create()
                </script>
                <button type="button" id = "btn-content"  class="btn btn-primary" onclick="submitfile()">上传</button>
            </form>
             </div>
        
        </section>
    </div>

            </div>
        </div>
            </div>
  </section>
     
    </div>

<!-- /js for banner section -->
<!-- /js files -->
</body>
      
<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<script src="js/bliss-slider.js" type="text/javascript"></script>
<script>
function submitfile(){
    var content=editor.txt.html();
    $("#content").attr("value",content);
	$.ajax({
        type: "POST",//方法类型
        url: "/fashion/ActionServlet?entity=Passage&method=addPassage" ,//url
        data: $('#con').serialize(),
        success: function (result) {
          // console.log(result);//打印服务端返回的数据(调试用)
            if (result == "success") {
                alert("上传成功");
                //window.location.href = "/fashion/owner1.jsp";
            }
            else if(result == "pfail")
            {
            	alert("上传内容失败");
            }else if(result == "cfail")
            {
            	alert("上传标签失败");
            }else if(result == "pfail")
            {
            	alert("上传内容失败");
            }
        },
        error : function() {
            alert("上传失败");
        }
    });
}
//$("#btn-content").click(function(){
//            alert("ss");
//            var content=editor.txt.html();
//            var title=$("#title1").val();
//            var jianjie=$("#jianjie").val();
//            var cType=$("input [type='radio']:checked").val();
//            alert(cType);
//            alert(title);
//            $.ajax({ url: "", 
//                    type:"post"
//                    data: {Title:title,Title2:jianjie,coType:cType,text:content}, 
//                    success: function(){
//                    $(this).addClass("done");
//                alert("成功");
//                    }
//                    Error:function(){
//                        alert("失败");
//                    }
//        });
//
//        });

/*function submitfile(){
    var content=editor.txt.html();
    $("#content").attr("value",content);
    
}*/

$(".modify-1").click(function(){
    $("#change-nickname").show();
});
$(".cancel-editor").click(function(){
    $("#change-nickname").hide();
});
    
 $(".modify-2").click(function(){
        $("#change-persontitle").show();
    });
$(".cancel-editor").click(function(){
        $("#change-persontitle").hide();
});
    
$(function() {
    $("#slider").blissSlider({
        auto: 1,
        transitionTime: 500,
        timeBetweenSlides: 4000
    });
}); 
 
   
//$("#btn-content").click(function getValue() ){
//                        alert("s");
//			var html1 = editor.txt.html();
//    alert(html1);
//			var a = document.getElementById("title").value;
//			var b = document.getElementById("simplecontent").value;
//            var c = $("input [type='radio']:checked").val();
//			var img = document.getElementById("timg").value;
//			var imgName = img.split("\\")[2];
////			var paper = {};
////			paper["title"] = a;
////			paper["imgname"] = imgName;
////			paper["simplecontent"] = b;
////            paper["kinds"] = c;
////			paper["maintxt"] = html1;
////			var testjson = JSON.stringify(paper);
////			alert(testjson);
//			//发送数据
//			$.ajax({
//				cache : false,
//				type : "POST", 
//                url : "",
////				url : "InsertTextServlet",
//				data : {
//					title : a,
//					imgname : imgName,
//					simplecontent : b,
//                    kinds : c,
//					maintxt : html1
//				},
//				dateType : "json",
//				async : true,
//				success : function(data) {
//					if (data.status == 'success') {
//						alert("提交成功");
//					} else if (data.status == 'fail') {
//						swal({
//							icon : "error",
//							text : "提交失败，请重试"
//						});
//					}
//				},
//			});
//			location.reload();
//		}       
</script>

    
</html>